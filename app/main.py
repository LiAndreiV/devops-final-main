import numpy as np
import pickle
import csv
from sklearn.tree import DecisionTreeClassifier
from flask import Flask, request, render_template, redirect

FILE_NAME = "spambase.data"


class DecisionTree():

    def __init__(self):
        if os.path.exists('dt_model.pickle'):
            self._load_model()
        else:
            self._train_model()

    def _load_model(self):
        with open('dt_model.pickle', 'rb') as f:
            self.dt_model = pickle.load(f)

    def _train_model(self):
        with open(FILE_NAME, "r") as f:
            data = list(csv.reader(f, delimiter=","))

        data = np.array(data, dtype=np.float32)

        # split into X and y
        n_samples, n_features = data.shape
        n_features -= 1

        X = data[:, 0:n_features]
        y = data[:, n_features]

        self.dt_model = DecisionTreeClassifier(max_depth=10)
        self.dt_model.fit(X, y)
        self._save_model()

    def _save_model(self):
        with open('dt_model.pickle', 'wb') as handle:
            pickle.dump(self.dt_model, handle)

    def predict(self, X):
        return self.dt_model.predict([X])


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')

    if request.method == 'POST':
        f = request.files['file']
        f.save(f.filename)
        return redirect(f'/predict?filename={f.filename}')


@app.route('/predict')
def predict():
    filename = request.args.get('filename')
    X = []
    with open(filename) as f:
        line = f.read().strip()

    for feature in line.split(','):
        X.append(feature)

    model = DecisionTree()
    prediction = model.predict(X)
    result = 'It is spam' if int(prediction) == 1 else 'It is not spam'

    return render_template('predict.html', text=result)


if __name__ == '__main__':
    app.run()
